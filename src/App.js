import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import '../node_modules/react-grid-layout/css/styles.css'
import '../node_modules/react-resizable/css/styles.css'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <a className="App-link"
            href="/"
            target="_blank"
            rel="noopener noreferrer">
            We are team akili
          </a>
        </header>
      </div>
    );
  }
}

export default App;
