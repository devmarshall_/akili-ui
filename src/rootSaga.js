import {
  all,
} from 'redux-saga/effects';
import * as authSaga from './services/auth/sagas';

function* loadApp() {
  yield console.log('Load App');
}

export default function* rootSaga() {
  yield all([
    authSaga.loginSaga(),
    authSaga.logoutSaga(),
    loadApp(),
  ]);
}
